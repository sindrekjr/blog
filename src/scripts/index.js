import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/index.scss'

/**
 * Global Constants
 */
const api = 'https://jsonplaceholder.typicode.com/';

/**
 * Variables
 */
let posts; 

/**
 * Elements
 */
let $app;
let $backBtn; 
let $searchBox; 

/**
 * Wait for the DOM to be ready
 * jQuery needs to cache to DOM :)
 */
$(document).ready(function() {
    $app = $('#app');
    $backBtn = $('#back-btn');
    $searchBox = $('#searchbox'); 

    $backBtn.click(ev => {
        listPosts();
    });

    $searchBox.keyup(ev => {
        // In the event that the search field has changed, we call listPosts with a filter function
        listPosts(post => post.title.includes($searchBox[0].value));
    });

    fetchPosts();
})


function fetchPosts() {
    fetch(api + 'posts/')
        .then(response => response.json())
        .then(json => {
            posts = json.map(p => new Post(p));
            listPosts(); 
        });
}

function postPost(post) {
    fetch("https://jsonplaceholder.typicode.com/users/" + post.userId)
        .then(response => response.json())
        .then(json => {
            $app.html(createElement(post, json));

            $searchBox.hide(); 
            $backBtn.show(); 
        });
}

function listPosts(filter) {
    let list = $('<ol>'); 

    if(filter) {
        posts.filter(filter).forEach(post => {
            list.append(createListElement(post))
        });
    } else {
        posts.forEach(post => list.append(createListElement(post)));
    }
    $app.html(list);

    $searchBox.show(); 
    $backBtn.hide(); 
}

function createListElement(post) {
    return $(`
        <li id=${post.id}>
            <a href="#">${post.title}</a>
        </li>
    `).click(ev => {
        postPost(post);
    });
}

function createElement(post, user) {
    return `
        <h5>
            ${post.title}
        </h5>
        <p>
            ${post.body}
        </p>
        <div>
            ${user.name}
            <br>
            ${user.email}
            <br>
            ${user.phone}
            <br>
            <a href="">
                ${user.website}
            </a>
        </div>
    `;
}

const Post = function(obj) {
    Object.defineProperties(this, {
        id: {
            value: obj.id,
            writable: false
        },
        userId: {
            value: obj.userId,
            writable: false
        },
        title: {
            value: obj.title,
            writable: false
        },
        body: {
            value: obj.body,
            writable: false
        }
    });    
}
