# jQuery Demo

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

## Table of Contents

- [Install](#install)
- [Develop](#develop)
- [Build](#build)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```bash
npm install
```

## Develop

Start the Webpack development server.

```bash
npm start
```

## Build

Create a minified, production build of the project for publication.

```bash
npm run build
```
